namespace NumberphileApi.Services
{
    public enum HttpVerb
    {
        Get, Post, Put, Delete
    }
}