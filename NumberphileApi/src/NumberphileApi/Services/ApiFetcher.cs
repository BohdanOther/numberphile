﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace NumberphileApi.Services
{
    public class EncodingProviderWrapper : EncodingProvider
    {
        public override Encoding GetEncoding(int codepage)
        {
            return Encoding.UTF8;
        }

        public override Encoding GetEncoding(string name)
        {
            return Encoding.UTF8;
        }
    }

    public class ApiFetcher
    {
        private const string defaultApiAddress = "";

        public static async Task<string> CallApi(HttpVerb requestType, string url = defaultApiAddress)
        {
            Encoding.RegisterProvider(new EncodingProviderWrapper());

            var client = new HttpClient();
            string result = null;

            if (requestType == HttpVerb.Get)
            {
                try
                {
                    var response = await client.GetAsync(url);
                    result = await response.Content.ReadAsStringAsync();
                }
                catch (Exception e)
                {
                    var a = e.ToString();
                    throw;
                }
            }

            return result;
        }
    }
}
