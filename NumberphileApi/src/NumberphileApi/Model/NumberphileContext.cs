﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NumberphileApi.Model
{
    public class NumberphileContext : DbContext
    {
        public NumberphileContext(DbContextOptions<NumberphileContext> options)
            : base(options)
        {
        }

        public DbSet<Fact> Facts { get; set; }
    }
}
