﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NumberphileApi.Model
{
    public interface IRepository<T>
    {
        T Add(T item);
        IEnumerable<T> GetAll();
        T Find(long key);
        void Remove(long key);
        void Update(T item);
    }
}
