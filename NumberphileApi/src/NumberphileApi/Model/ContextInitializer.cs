﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NumberphileApi.Model
{
    public class ContextInitializer
    {
        public static List<Fact> GetInitialData()
        {
            return new List<Fact>
            {
                new Fact {Number = "12", Text = "12 is the age when I saw the ocean for the first time."},
                new Fact {Number = "13", Text = "13 is my favorite number."},
                new Fact {Number = "19", Text = "19 plays important role in Dark Tower by Stephen King."},
                new Fact {Number = "98", Text = "Is my mark for the Java course."}
            };
        } 
    }
}
