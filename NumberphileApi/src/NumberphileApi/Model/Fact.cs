﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NumberphileApi.Model
{
    public class Fact
    {
        public const string PersonalFactType = "personal";
        public const string RandomNumberFlag = "random";

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty(PropertyName = "id")]
        public long Key { get; set; }

        /// <summary>
        /// Can be "random" or have exponential form so is not a number
        /// </summary>
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "found")]
        public bool Found { get; set; } = true;

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = PersonalFactType;

        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }
    }
}
