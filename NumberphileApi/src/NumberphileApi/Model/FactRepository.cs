﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NumberphileApi.Model
{
    public class FactRepository : IRepository<Fact>
    {
        private readonly NumberphileContext _context;

        public FactRepository(NumberphileContext context)
        {
            _context = context;

            if (!_context.Facts.Any())
               ContextInitializer.GetInitialData().ForEach(fact => Add(fact));
        }

        public IEnumerable<Fact> GetAll()
        {
            return _context.Facts.ToList();
        }

        public Fact Add(Fact fact)
        {
            var addedFact = _context.Facts.Add(fact);
            _context.SaveChanges();
            return addedFact.Entity;
        }

        public Fact Find(long key)
        {
            return _context.Facts.FirstOrDefault(t => t.Key == key);
        }

        public void Remove(long key)
        {
            var entity = _context.Facts.First(t => t.Key == key);
            _context.Facts.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Fact item)
        {
            _context.Facts.Update(item);
            _context.SaveChanges();
        }
    }
}
