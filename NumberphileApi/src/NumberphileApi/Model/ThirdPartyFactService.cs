﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NumberphileApi.Services;

namespace NumberphileApi.Model
{
    public interface IThirdPartyFactService
    {
        Task<Fact> GetFact(string number, string type, bool json = true, bool fragment = false);
    }

    public class ThirdPartyFactService : IThirdPartyFactService
    {
        private const string ApiUrlBase = @"http://numbersapi.com";

        public async Task<Fact> GetFact(string number, string type, bool json = true, bool fragment = false)
        {
            var url = $@"{ApiUrlBase}/{number}/{type}?json" + (fragment ? "&fragment" : "");
            var s = await ApiFetcher.CallApi(HttpVerb.Get, url);
            return JsonConvert.DeserializeObject<Fact>(s);
        }
    }
}
