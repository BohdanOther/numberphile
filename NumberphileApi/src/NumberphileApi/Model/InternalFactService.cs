﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.String;

namespace NumberphileApi.Model
{
    public interface IInternalFactService
    {
        IEnumerable<Fact> AllPersonalFacts(string targetNumber, string userId);
        Fact SaveFact(string userId, Fact fact);
        void DeleteFact(string userId, long factId);
    }
    public class InternalFactService : IInternalFactService
    {
        private readonly IRepository<Fact> _factRepository;

        public InternalFactService(IRepository<Fact> factRepository)
        {
            _factRepository = factRepository;
        }

        public IEnumerable<Fact> AllPersonalFacts(string targetNumber, string userId)
        {
            var availableFacts = _factRepository.GetAll()
                .Where(fact => IsNullOrEmpty(fact.UserId)
                    || fact.UserId.Equals(userId, StringComparison.CurrentCultureIgnoreCase));

            if (targetNumber.Equals(Fact.RandomNumberFlag))
                return availableFacts;

            return availableFacts.Where(fact => fact.Number == targetNumber);
        }

        public Fact SaveFact(string userId, Fact fact)
        {
            fact.UserId = userId;
            return _factRepository.Add(fact);
        }

        public void DeleteFact(string userId, long factId)
        {
            var targetFact = _factRepository.Find(factId);
            if (targetFact.UserId == userId)
                _factRepository.Remove(targetFact.Key);
        }
    }
}
