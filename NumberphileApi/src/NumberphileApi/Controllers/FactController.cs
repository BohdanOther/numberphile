﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NumberphileApi.Model;
using NumberphileApi.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace NumberphileApi.Controllers
{
    [Route("api/[controller]")]
    public class FactController : Controller
    {
        private readonly IInternalFactService _internalFactService;
        private readonly IThirdPartyFactService _thirdPartyFactService;

        public FactController(IInternalFactService internalFactService, IThirdPartyFactService thirdPartyFactService)
        {
            _internalFactService = internalFactService;
            _thirdPartyFactService = thirdPartyFactService;
        }

        [HttpGet("number/{number=random}/{type}/{json?}/{fragmet?}", Name = "GetWithFilters")]
        public async Task<Fact> GetWithFilters(string number, string type, bool json = true, bool fragment = false)
        {
            return await _thirdPartyFactService.GetFact(number, type, json, fragment);
        }

        [HttpGet("personal/{userId}/{number?}", Name = "GetPersonal")]
        public IEnumerable<Fact> GetPersonal(string userId, string number = Fact.RandomNumberFlag)
        {
            return _internalFactService.AllPersonalFacts(number, userId);
        }

        [HttpPost("personal/{userId}", Name = "SaveFact")]
        public Fact SaveFact(string userId, Fact fact)
        {
            return _internalFactService.SaveFact(userId, fact);
        }

        [HttpDelete("personal/{userId}/fact/{factId}", Name = "DeleteFact")]
        public void DeleteFact(string userId, long factId)
        {
            _internalFactService.DeleteFact(userId, factId);
        }
    }
}
