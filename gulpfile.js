"use strict"

var gulp = require('gulp');
var surge = require('gulp-surge');
var webserver = require('gulp-webserver');

gulp.task('webserver', function() {
    gulp.src('src')
        .pipe(webserver({
            port: 8080,
            livereload: {
                port: 8080
            },
            open: true
        }));
});

gulp.task('deploy', [], function() {
    return surge({
        project: './src',
        domain: 'numberphile.surge.sh'
    })
});